'use strict';

module.exports = function (server) {

 let ListService = server.models.List; //List nom de la collection

  ListService.findOrCreate({
      where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
        listName: 'companyList'
      }
    }, {
      listName: 'companyList',
      comment:'Liste des entreprises',
      options: [{
          key: 'CIE',
          optionName: 'Danone'
        },
        {
          key: 'CIE2',
          optionName: 'La Poste'
        },
        {
          key: 'CIE3',
          optionName: 'Thalès'
        },
        {
          key: 'CIE4',
          optionName: 'Autre'
        }
      ]
    }
  ).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
  ).catch(err => console.error(err))

  ListService.findOrCreate({
    where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
      listName: 'serviceList'
    }
  }, {
    listName: "serviceList",
    comment:'Nom des services',
    options: [
        { key: 'SER', optionName: 'Logistique' },
        { key: 'SER2', optionName: 'Livraison' },
        { key: 'SER3', optionName: 'Administratif' },
        { key: 'SER4', optionName: 'Autre' }
    ]
  }
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'chiefList'
  }
}, {
  listName: "chiefList",
  comment:'Liste des responsables de service',
  options: [
      { key: 'CHF', optionName: 'Gérard Durand' },
      { key: 'CHF2', optionName: 'Françoise Hénaff' },
      { key: 'CHF3', optionName: 'Fabrice Legall' },
      { key: 'CHF4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'driverList'
  }
}, {
  listName: "driverList",
  comment:'Liste des conducteurs',
  options: [
      { key: 'DRV', optionName: 'Julien Renard' },
      { key: 'DRV2', optionName: 'Ronan Legall' },
      { key: 'DRV3', optionName: 'Eloïse Pennec' },
      { key: 'DRV4', optionName: 'Annaïg Bernard' },
      { key: 'DRV5', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'drivingLicences'
  }
}, {
  listName: "drivingLicences",
  comment:'Liste des types de permis',
  options: [
      { key: 'DRLB', optionName: 'permis B' },
      { key: 'DRLC', optionName: 'permis C' },
      { key: 'DRLD', optionName: 'permis D' },
      { key: 'DRLBE', optionName: 'permis BE' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'vehiculeList'
  }
}, {
listName: "vehiculeList",
comment:'Liste des véhicules',
options: [
    { key: 'VEH', optionName: 'Renault trafic' },
    { key: 'VEH2', optionName: 'Citroën C3' },
    { key: 'VEH3', optionName: 'VW Transporter' },
    { key: 'VEH4', optionName: 'Peugeot 308' },
    { key: 'VEH5', optionName: 'PL TX-road' },
    { key: 'VEH6', optionName: 'Autre' }
]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'drivingConditionList'
  }
}, {
  listName: "drivingConditionList",
  comment:'Liste des raisons du déplacements',
  options: [
      { key: 'DCL', optionName: 'Personnel' },
      { key: 'DCL2', optionName: 'Livraison' },
      { key: 'DCL3', optionName: "Réunion à l'extérieur" },
      { key: 'DCL4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'timeSituationList'
  }
}, {
  listName: "timeSituationList",
  comment:'Liste de la situation par rapport à horaire prévu',
  options: [
      { key: 'TMS', optionName: 'Dans les temps' },
      { key: 'TMS2', optionName: 'En avance' },
      { key: 'TMS3', optionName: "En retard" },
      { key: 'TMS4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'driverFeelingList'
  }
}, {
  listName: "driverFeelingList",
  comment:'Liste de humeur du conducteur',
  options: [
      { key: 'DRFL', optionName: 'De bonne humeur' },
      { key: 'DRFL2', optionName: 'De mauvaise humeur' },
      { key: 'DRFL3', optionName: 'En pleine forme' },
      { key: 'DRFL4', optionName: 'fatigué' },
      { key: 'DRFL5', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'vehiculeTypeList'
  }
}, {
  listName: "vehiculeTypeList",
  comment:'Liste du type de véhicule',
  options: [
      { key: 'VHCT', optionName: "Véhicule de l'entreprise" },
      { key: 'VHCT2', optionName: 'Véhicule de location' },
      { key: 'VHCT3', optionName: 'Véhicule personnel' },
      { key: 'VHCT4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'chargeModeList'
  }
}, {
  listName: "chargeModeList",
  comment:'Liste des conditions de chargement',
  options: [
      { key: 'CMD1', optionName: 'vide' },
      { key: 'CMD2', optionName: 'en charge partielle' },
      { key: 'CMD3', optionName: 'en charge totale' },
      { key: 'CMD4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'parkingTypeList'
  }
}, {
  listName: "parkingTypeList",
  comment:'Liste du type de stationnement',
  options: [
      { key: 'PKGT', optionName: "parking" },
      { key: 'PKGT2', optionName: 'long du trottoir' },
      { key: 'PKGT3', optionName: 'stationnement régulier' },
      { key: 'PKGT4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'sinisterTypeList'
  }
}, {
  listName: "sinisterTypeList",
  comment:"Liste du type d'accident",
  options: [
      { key: 'SINT1', optionName: 'accident matériel' },
      { key: 'SINT2', optionName: 'accident corporel' },
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'weatherConditionList'
  }
}, {
  listName: "weatherConditionList",
  comment:'Liste des types de conditions météorologiques',
  options: [
      { key: 'WTH1', optionName: 'pluie' },
      { key: 'WTH2', optionName: 'temps sec' },
      { key: 'WTH3', optionName: 'neige' },
      { key: 'WTH4', optionName: 'brouillard' },
      { key: 'WTH5', optionName: 'verglas' },
      { key: 'WTH6', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'environnementList'
  }
}, {
  listName: "environnementList",
  comment:"Liste du type d'environnement",
  options: [
      { key: 'ENV', optionName: 'zone urbaine' },
      { key: 'ENV2', optionName: 'zone rurale' },
      { key: 'ENV3', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'accidentConfigList'
  }
}, {
  listName: "accidentConfigList",
  comment:"Liste du type de configuration ddes lieux de l'accident",
  options: [
      { key: 'ACCF', optionName: 'ligne droite' },
      { key: 'ACCF2', optionName: 'courbe' },
      { key: 'ACCF3', optionName: 'plat' },
      { key: 'ACCF4', optionName: 'pente' },
      { key: 'ACCF5', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'thirdTypeList'
  }
}, {
  listName: "thirdTypeList",
  comment:'Liste du type de tiers impliqué dans accident',
  options: [
      { key: 'TT', optionName: 'personne' },
      { key: 'TT2', optionName: 'véhicule léger' },
      { key: 'TT3', optionName: 'véhicule lourd' },
      { key: 'TT4', optionName: '2 roues non motorisé' },
      { key: 'TT5', optionName: '2 roues motorisé' },
      { key: 'TT6', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'thirdSinisterList'
  }
}, {
  listName: "thirdSinisterList",
  comment:"Liste du type d'accident subi par le tiers",
  options: [
      { key: 'TS', optionName: 'accident matériel' },
      { key: 'TS2', optionName: 'accident corporel' },
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'thirdCircumstanceList'
  }
}, {
  listName: "thirdCircumstanceList",
  comment:'Liste du type de situation du vehicule du tiers',
  options: [
      { key: 'TC', optionName: 'stationnement' },
      { key: 'TC2', optionName: 'en circulation' },
      { key: 'TC3', optionName: 'en marche arrière (sauf si mise à quai)' },
      { key: 'TC4', optionName: 'quittait un stationnement' },
      { key: 'TC5', optionName: "à l'arrêt" },
      { key: 'TC6', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'dommagesGravityList'
  }
}, {
  listName: "dommagesGravityList",
  comment:'Liste du type de gravité des dommages',
  options: [
      { key: 'DG1', optionName: 'peu grave' },
      { key: 'DG2', optionName: 'grave' },
      { key: 'DG3', optionName: 'très grave' },
      { key: 'DG4', optionName: 'Autre' }
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'impactPointList'
  }
}, {
  listName : "impactPointList",
  comment:"Liste des points d'impact",
  options: [                
      { key: "AVG", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/avt-G120.png"},
      { key: "AVM", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/avt-M120.png"},
      { key : "AVD", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/avt-D120.png"},
      { key : "LatG", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/lat-g120.png"},
      { key : "Toit", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/toit120.png"},
      { key : "LatD", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/lat-d120.png"},
      { key : "ARG", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/arr-g120.png"},
      { key : "ARM", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/arr-m120.png"},
      { key : "ARD", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/arr-d120.png"},
      { key : "PB-AV", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/pb-avt120.png"},
      { key: "PB-AR", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/pb-arr120.png"},
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

ListService.findOrCreate({
  where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    listName: 'collisionTypeList'
  }
}, {
  listName : "collisionTypeList",
  comment:'Liste du type de collision',
  options: [
      { key: "ColFront", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/COL-FRONT120.png"},
      { key: "ColLat", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/COL-LAT120.png"},
      { key : "ColAr", optionName : "https://bitbucket.org/EmilieBellier/list-survey/downloads/COL-ARR120.png"},
  ]
}
).then(list => console.log('list de checkList ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))


};
